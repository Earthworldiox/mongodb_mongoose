/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
// const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main() {
  // Update
  // const room = await Room.findById('621b1f8fe849e330f74d1293')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({})
  console.log(JSON.stringify(building))
}

main().then(() => {
  console.log('Finish')
})
