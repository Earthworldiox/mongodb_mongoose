/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
// const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main() {
  const newInformaticsBuilding = await Building.findById('623075f8ba32471561f6efd9')
  const room = await Room.findById('623075f8ba32471561f6efde')
  const informaticsBuilding = await Building.findById(room.building)

  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)

  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})